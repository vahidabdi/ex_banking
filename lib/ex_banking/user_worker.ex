defmodule ExBanking.UserWorker do
  use GenServer

  alias ExBanking.Service.Money

  def start_link(user_name) do
    GenServer.start_link(__MODULE__, user_name, name: via_tuple(user_name))
  end

  def deposit(pid, amount, currency) do
    GenServer.call(pid, {:deposit, amount, currency})
  end

  def withdraw(pid, amount, currency) do
    GenServer.call(pid, {:withdraw, amount, currency})
  end

  @impl true
  def init(user_name) do
    state = %{
      user_name: user_name,
      balance: %{}
    }

    {:ok, state}
  end

  @impl true
  def handle_call({:deposit, amount, currency}, _from, state) do
    new_amount =
      case get_in(state, [:balance, currency]) do
        nil -> Money.new(amount)
        %{"amount" => current_amount} -> Money.add(current_amount, amount)
      end

    new_state = put_in(state, [:balance, currency], %{"amount" => new_amount})

    {:reply, new_amount, new_state}
  end

  @impl true
  def handle_call({:withdraw, amount, currency}, _from, state) do
    with(
      %{"amount" => current_amount} <- get_in(state, [:balance, currency]),
      new_amount = Money.sub(current_amount, amount),
      false <- Decimal.negative?(new_amount)
    ) do
      new_state = put_in(state, [:balance, currency], %{"amount" => new_amount})
      {:reply, {:ok, new_amount}, new_state}
    else
      _ -> {:reply, {:error, :not_enough_money}, state}
    end
  end

  defp via_tuple(user_name) do
    {:via, Registry, {:user_name, user_name}}
  end
end
