defmodule ExBanking.TransactionWorker do
  use GenServer

  alias ExBanking.Service.User

  def start_link({from, to}) do
    GenServer.start_link(__MODULE__, [], name: via_tuple({from, to}))
  end

  def send(pid, from, to, amount, currency) do
    GenServer.call(pid, {:send, from, to, amount, currency})
  end

  @impl true
  def init(_) do
    {:ok, nil}
  end

  @impl true
  def handle_call({:send, from, to, amount, currency}, _from, state) do
    result =
      case {User.get_user(from), User.get_user(to)} do
        {[], _} ->
          {:error, :sender_does_not_exist}

        {_, []} ->
          {:error, :receiver_does_not_exist}

        {[{_from_user, _}], [{_to_user, _}]} ->
          with {:ok, from_user_new_balance} <- User.withdraw(from, amount, currency) do
            {:ok, to_user_new_balance} = User.deposit(to, amount, currency)
            {:ok, from_user_new_balance, to_user_new_balance}
          else
            _ -> {:error, :not_enough_money}
          end
      end

    {:reply, result, state}
  end

  defp via_tuple({from, to}) do
    {:via, Registry, {:transaction, {from, to}}}
  end
end
