defmodule ExBanking.UserSupervisor do
  use DynamicSupervisor
  alias ExBanking.UserWorker

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_child(user_name) do
    DynamicSupervisor.start_child(
      __MODULE__,
      %{id: UserWorker, start: {UserWorker, :start_link, [user_name]}, restart: :transient}
    )
  end
end
