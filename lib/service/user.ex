defmodule ExBanking.Service.User do
  alias ExBanking.UserWorker
  alias ExBanking.TransactionWorker

  defdelegate create_user(user_name), to: UserWorker, as: :start_link

  def deposit(user_name, amount, currency) do
    with [{pid, _}] <- get_user(user_name) do
      {:ok, UserWorker.deposit(pid, amount, currency)}
    else
      [] ->
        {:error, :user_does_not_exist}
    end
  end

  def withdraw(user_name, amount, currency) do
    with [{pid, _}] <- get_user(user_name),
         {:ok, new_amount} <- UserWorker.withdraw(pid, amount, currency) do
      {:ok, new_amount}
    else
      {:error, :not_enough_money} -> {:error, :not_enough_money}
      [] -> {:error, :user_does_not_exist}
    end
  end

  def get_balance(user_name, currency) do
    with [{pid, _}] <- get_user(user_name) do
      balance = get_state(pid).balance
      {:ok, Map.get(balance, currency, nil)}
    else
      [] ->
        {:error, :user_does_not_exist}
    end
  end

  def send(from, to, amount, currency) do
    pid = transaction_checkout({from, to})
    TransactionWorker.send(pid, from, to, amount, currency)
  end

  def get_state(pid) do
    :sys.get_state(pid)
  end

  def get_user(user_name) do
    Registry.lookup(:user_name, user_name)
  end

  defp transaction_checkout({from, to}) do
    case TransactionWorker.start_link({from, to}) do
      {:ok, pid} -> pid
      {:error, {:already_started, pid}} -> pid
    end
  end
end
