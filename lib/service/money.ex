defmodule ExBanking.Service.Money do
  @enforce_keys [:amount]
  defstruct [:amount]

  def new(amount) do
    Decimal.cast(amount) |> Decimal.round(2)
  end

  def add(a, b) do
    Decimal.add(cast(a), cast(b)) |> Decimal.round(2)
  end

  def sub(a, b) do
    Decimal.sub(cast(a), cast(b)) |> Decimal.round(2)
  end

  defp cast(%Decimal{} = number), do: number
  defp cast(number), do: Decimal.cast(number)
end
